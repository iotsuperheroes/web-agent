# Web Agent

Sensor Gateway for web enabled devices

### Get prepared

- `npm i`

### Get going

- `npm start`
- `http://localhost:3000`

### Get building

- `npm run build`

## LICENCE

MIT