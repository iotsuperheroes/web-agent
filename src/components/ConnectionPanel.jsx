import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';
import Chip from 'material-ui/Chip';
import { red200, yellow300, green300, grey300 } from 'material-ui/styles/colors';
import { socketStatus as statuses } from '../services/tiip-session';

const marginStyle = {
  marginLeft: 10,
};

export default class ConnectionPanel extends React.PureComponent {

  // changeServerIp = (ev) => this.props.setServerIp(ev.target.value);
  // changeUsername = (ev) => this.props.setUsername(ev.target.value);
  onConnectPress = () => {
    // console.log('Connect pressed:', this.serverIp);
    this.props.setServerIp(this.serverIp.input.value);
    this.props.setUsername(this.username.input.value);
    this.props.appConnect(this.serverIp.input.value, this.username.input.value, this.secure.checked);
  }

  connectTitle = () => {
    switch (this.props.socketStatus) {
      case statuses.notConnected: return 'Connect';
      case statuses.connected: return 'Disconnect';
      case statuses.reconnecting: return 'Disconnect';
      case statuses.closed: return 'Connect';
      case statuses.unableToConnect: return 'Connect';
      default: return 'Connect';
    }
  }

  connectMessage = () => {
    switch (this.props.socketStatus) {
      case statuses.notConnected: return 'Not connected';
      case statuses.connected: return 'Connected';
      case statuses.reconnecting: return 'Reconnecting...';
      case statuses.closed: return 'Connection closed';
      case statuses.unableToConnect: return 'Unable to connect';
      default: return '';
    }
  }

  connectColor = () => {
    switch (this.props.socketStatus) {
      case statuses.notConnected: return grey300;
      case statuses.connected: return green300;
      case statuses.reconnecting: return yellow300;
      case statuses.closed: return grey300;
      case statuses.unableToConnect: return red200;
      default: return '';
    }
  }

  render() {
    // const { username, serverIp } = this.props;
    const chipStyle = {
      ...marginStyle,
      marginTop: 5,
      display: 'inline-block',
      background: this.connectColor(),
    };
    return (
      <div style={{ marginBottom: 20 }}>
        <Checkbox
          label="Secure"
          ref={c => this.secure = c} // eslint-disable-line
          style={{ ...marginStyle, display: 'inline-block', width: 'auto' }}
        />
        <TextField
          floatingLabelText="Server address"
          defaultValue="iim2m.com"
          style={marginStyle}
          ref={c => this.serverIp = c} // eslint-disable-line
          // value={serverIp}
          // onChange={this.changeServerIp}
        />
        <TextField
          floatingLabelText="Username"
          style={marginStyle}
          ref={c => this.username = c} // eslint-disable-line
          // value={username}
          // onChange={this.changeUsername}
        />
        <RaisedButton
          label={this.connectTitle()}
          style={marginStyle}
          onTouchTap={this.onConnectPress}
        />
        <Chip style={chipStyle}>{this.connectMessage()}</Chip>
      </div>
    );
  }
}
