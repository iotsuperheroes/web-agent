import React, { PropTypes } from 'react';
import IPropTypes from 'react-immutable-proptypes';
// import { List } from 'immutable';
import { List as MuiList } from 'material-ui/List';
import Chip from 'material-ui/Chip';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import ListItemToggle from 'react-material-ui-extras/lib/ListItemToggle';
import sensorConf from '../services/sensors';
import { selectedWeatherNowAt } from '../services/smhi';

const styleInline = {
  display: 'inline-block',
  margin: '10px 5px',
};

export default class ConnectionPanel extends React.PureComponent {

  static propTypes = {
    sensors: IPropTypes.map.isRequired,
    toggleSensor: PropTypes.func.isRequired,
    setSensorExtra: PropTypes.func.isRequired,
  }

  fetchWeatherData = (sensor, pos) => {
    const { setSensorExtra } = this.props;
    selectedWeatherNowAt(pos.get('lat'), pos.get('lon')).then(data =>
      setSensorExtra({
        sensor,
        key: 'weather',
        data,
      }),
    );
  }

  renderSensorInfo = (sensorId) => {
    const { sensors } = this.props;
    switch (sensorId) {
      case 'orientation':
      case 'motion':
      case 'location': {
        const last = sensors.getIn([sensorId, 'last']);
        const weather = sensors.getIn([sensorId, 'weather']);
        if (last && !last.isEmpty() && last.every(v => v)) {
          return (<div>
            <Chip style={styleInline}>{last.toList().map(v => v.toFixed(4)).join(', ')}</Chip>
            <RaisedButton style={styleInline} label="Weather" onTouchTap={this.fetchWeatherData.bind(this, sensorId, last)} />
            {weather &&
              <div style={styleInline}>
                <Chip style={styleInline}>
                  {`Temp: ${weather.get('t')} deg C`}
                </Chip>
                <Chip style={styleInline}>
                  {`Rel.hum: ${weather.get('r')}%`}
                </Chip>
                <Chip style={styleInline}>
                  {`Wind speed: ${weather.get('ws')} m/s`}
                </Chip>
              </div>
            }
          </div>);
        }
        return (<div>
          <Chip>{'No data'}</Chip>
        </div>);
      }
      case 'camera': {
        const last = sensors.getIn([sensorId, 'last']);
        if (last && typeof last === 'string') return <img src={last} />;
        return <Chip>{'No data'}</Chip>;
      }
      default:
        return false;
    }
  }

  render() {
    const { sensors, toggleSensor } = this.props;
    return (
      <MuiList>
        <Divider />
        {sensorConf.map((s, key) =>
          <div>
            <ListItemToggle
              primaryText={s.get('name')}
              icon={s.get('icon')}
              onToggle={toggleSensor.bind(this, key, !sensors.getIn([key, 'active']))}
              toggled={sensors.getIn([key, 'active'])}
            >
              {this.renderSensorInfo(key)}
            </ListItemToggle>
            <Divider />
          </div>,
        )}
      </MuiList>
    );
  }
}
