import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import MuiIcon from 'react-material-ui-extras/lib/MuiIcon';
import ConnectionPanel from '../containers/ConnectionPanel';
import SensorList from '../containers/SensorList';
import Theme from '../styles/theme1';
import conf from '../../client.config';

import '../styles/global.css';

const h1Style = {
  margin: '0 0 0 15px',
  color: 'white',
  fontSize: 24,
  fontWeight: 400,
  lineHeight: '48px',
  display: 'inline',
};

export default class App extends React.PureComponent {

  static propTypes = {
  };

  render() {
    return (
      <MuiThemeProvider muiTheme={Theme}>
        <div>
          <AppBar
            iconElementLeft={<div>
              <MuiIcon plain style={{ verticalAlign: 'sub' }} icon="phonelink_setup" color="white" />
              <h1 style={h1Style}>{conf.title}</h1>
            </div>}
          />
          <ConnectionPanel />
          <SensorList />
        </div>
      </MuiThemeProvider>
    );
  }
}
