import http from 'http';
import express from 'express';
import webpack from 'webpack';
import path from 'path';
import wdm from 'webpack-dev-middleware';
import whm from 'webpack-hot-middleware';
import webpackConfig from '../webpack.config.babel';

const compiler = webpack(webpackConfig);
const port = 3000;

const httpServer = http.createServer();
const app = express();

httpServer.on('request', app);

const devMiddleware = wdm(compiler, {
  noInfo: true,
  publicPath: webpackConfig.output.publicPath,
});

app.use(devMiddleware);
app.use(whm(compiler));

app.get('*', (req, res) => {
  const index = devMiddleware.fileSystem.readFileSync(
    path.join(webpackConfig.output.path, 'index.html'),
  );
  res.end(index);
});

httpServer.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
});

export default { app, httpServer };
