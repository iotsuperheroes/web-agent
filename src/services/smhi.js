import { fromJS, List } from 'immutable';

const domain = 'opendata-download-metfcst.smhi.se';
const url = `http://${domain}/api/category/pmp2g/version/2/geotype/point`;

const desiredData = fromJS(['t', 'ws', 'r']);

export const weatherAt = (lat, lon) =>
  fetch(`${url}/lon/${lon.toFixed(3)}/lat/${lat.toFixed(3)}/data.json`)
    .then(reply => reply.json())
    .then(data => fromJS(data).get('timeSeries', List()));

export const weatherNowAt = (lat, lon) => {
  const hNow = (new Date()).getHours();
  return weatherAt(lat, lon).then(timeSeries =>
    timeSeries
      .find(atTime => (new Date(atTime.get('validTime'))).getHours() === hNow, fromJS({}))
      .get('parameters', List())
      .toMap()
      .mapKeys((k, v) => v.get('name')),
  );
};

export const selectedWeatherNowAt = (lat, lon) =>
  weatherNowAt(lat, lon).then(o =>
    o
      .filter((v, k) => desiredData.includes(k))
      .map(v => v.getIn(['values', 0], '')),
  );
