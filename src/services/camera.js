import 'webrtc-adapter';

const videoWidth = 300;
const videoHeight = videoWidth * 3 / 4;
let fps = 4;
let frameDelay = 1000 / fps;
let quality = 0.2;
const videoConf = {
  video: {
    mandatory: {
      minWidth: videoWidth / 3,
      maxWidth: videoWidth,
    },
  },
};

let stream;
let src;
const video = document.createElement('video');
video.width = videoWidth;
video.height = videoHeight;
video.autoPlay = true;
const canvas = document.createElement('canvas');
canvas.width = videoWidth;
canvas.height = videoHeight;
const ctx = canvas.getContext('2d');

let timer;
let extCallback = () => {};

const callback = () => {
  ctx.drawImage(video, 0, 0, videoWidth, videoHeight);
  const lastPoint = canvas.toDataURL('image/jpeg', quality);
  // console.log('Got Frame:', lastPoint, extCallback);
  extCallback(lastPoint, 'camera');
};

export const init = (func, _fps, _quality) => {
  if (_fps) {
    fps = _fps;
    frameDelay = 1000 / fps;
  }
  if (_quality) quality = _quality;
  extCallback = func;
  navigator.mediaDevices.getUserMedia(videoConf)
    .then((videoStream) => {
      stream = videoStream;
      if (window.URL) {
        src = window.URL.createObjectURL(videoStream);
      } else if (window.webkitURL) {
        src = window.webkitURL.createObjectURL(videoStream);
      } else {
        Promise.reject(new Error('Could not create URL from stream'));
      }
      video.src = src;
      timer = setInterval(callback, frameDelay);
      console.log('mediaDevices video started');
    });
};

export const stop = () => {
  clearInterval(timer);
  if (stream) {
    stream.getVideoTracks()[0].stop();
    stream = undefined;
  }
  src = undefined;
  extCallback = () => {};
  console.log('mediaDevices video stopped');
};
