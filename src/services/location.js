import { OrderedMap } from 'immutable';

let minDelay = 1000;
let timer;

let lastTimestamp = Date.now();
let extCallback = () => {};

const callback = (ev) => {
  const time = Date.now();
  if (time < lastTimestamp + minDelay) return;
  const lastPoint = OrderedMap({ lat: ev.coords.latitude, lon: ev.coords.longitude });
  lastTimestamp = time;
  extCallback(lastPoint, 'location');
};

export const init = (func, _minDelay) => {
  if (_minDelay) minDelay = _minDelay;
  extCallback = func;
  if (navigator.geolocation) {
    timer = setInterval(
      () => { navigator.geolocation.getCurrentPosition(callback); },
      minDelay,
    );
  } else {
    throw new Error('No device location support');
  }
  console.log('geolocation started');
};

export const stop = () => {
  clearInterval(timer);
  extCallback = () => {};
  console.log('geolocation stopped');
};
