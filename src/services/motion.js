import { OrderedMap } from 'immutable';

let minDelay = 50;
let maxDelay = 400;
let epsilon = 3;

let lastPoint;
let lastTimestamp = Date.now();
let extCallback = () => {};

const callback = (ev) => {
  const time = Date.now();
  if (time < lastTimestamp + maxDelay) {
    if (time < lastTimestamp + minDelay) {
      return;
    }
    const diff = lastPoint
      ? lastPoint.reduce((r, v, key) => r + Math.abs(ev[key] - v), 0)
      : epsilon + 1;
    if (diff < epsilon) {
      return;
    }
  }
  lastPoint = OrderedMap({ x: ev.x, y: ev.y, z: ev.z });
  lastTimestamp = time;
  extCallback(lastPoint, 'motion');
};

export const init = (func, _minDelay, _maxDelay, _epsilon) => {
  if (window.DeviceMotionEvent) {
    if (_minDelay) minDelay = _minDelay;
    if (_maxDelay) maxDelay = _maxDelay;
    if (_epsilon) epsilon = _epsilon;
    extCallback = func;
    window.addEventListener('devicemotion', callback, false);
  } else {
    throw new Error('No device motion support');
  }
  console.log('devicemotion started');
};

export const stop = () => {
  lastPoint = undefined;
  extCallback = () => {};
  window.removeEventListener('devicemotion', callback);
  console.log('devicemotion started');
};
