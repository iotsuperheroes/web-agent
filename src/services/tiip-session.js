import TiipSession from 'archmage-client';
// import TiipSession from 'tiip-client-js';
import { List, Iterable } from 'immutable';
import { deviceId } from '../services/device';
// import clientConf from '../../client.config';

export const socketStatus = {
  notConnected: 0,
  connected: 1,
  reconnecting: 2,
  closed: 3,
  unableToConnect: 4,
};

const session = new TiipSession();
// channel = dataChannels.get(sensor)

export const pubWatchdog = (username, channel, online) => {
  // console.log('pubWatchdog');
  if (!session.isOpen()) return Promise.reject(new Error('Session is not open'));
  return session.socket.pub(
    [deviceId, username], // payload
    channel, // channel
    undefined, // subChannel
    online ? 'online' : 'offline', // signal
  );
};

export const pubData = (data, channel, username) => {
  console.log('pubData');
  if (!session.isOpen()) return Promise.reject(new Error('Session is not open'));
  try {
    return session.socket.pub(
      Iterable.isIterable(data) ? data.toList().toJS() : [data], // payload
      channel, // channel
      deviceId, // subChannel
      username, // signal
      [deviceId], // source
    );
  } catch (err) {
    console.warn(err);
    return false;
  }
};

export const disconnect = async (username, watchdogRid) => {
  // console.log('disconnect');
  try {
    await pubWatchdog(username, watchdogRid, false);
  } catch (err) {
    console.warn(err);
  }
  return session.logout();
};

export const connect = (serverIp, onRelogin, onClose, secure) => {
  // console.log('connect');
  session.connect(`${secure ? 'wss' : 'ws'}://${serverIp}/wsh`, undefined, { onRelogin });
  session.socket.ws.onClose(onClose);
};

export const auth = async (userId, pw, tenant) => {
  // console.log('auth:', userId, pw, tenant);
  try {
    const authReply = await session.auth(userId, pw, tenant);
    // console.log('authReply:', authReply.toJS());
    return authReply.getIn(['payload', 0]);
  } catch (err) {
    return Promise.reject(new Error('Authentication or initialization failed'));
  }
};

export const readChannels = async (thingRid) => {
  // console.log('readChannels:', thingRid);
  try {
    const channelsReply = await session.socket.req('conf', 'readDataChannel', {
      providerRid: thingRid,
    });
    const channels = channelsReply.get('payload', List()).filter(v => v.get('type'))
      .toMap().mapKeys((key, v) => v.get('type'));
    // console.log('channels:', channels.toJS());
    return channels;
  } catch (err) {
    return Promise.reject(err);
  }
};

export default session;
