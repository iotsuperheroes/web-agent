import { fromJS } from 'immutable';
import * as orientation from './orientation';
import * as motion from './motion';
import * as location from './location';
import * as camera from './camera';

export default fromJS({
  location: {
    name: 'Location',
    icon: 'location_on',
    sensor: location,
  },
  orientation: {
    name: 'Orientation',
    icon: 'screen_rotation',
    sensor: orientation,
  },
  motion: {
    name: 'Motion',
    icon: 'directions_walk',
    sensor: motion,
  },
  camera: {
    name: 'Camera',
    icon: 'videocam',
    sensor: camera,
  },
});
