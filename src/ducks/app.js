import { fromJS, Map } from 'immutable';
import { createAction, handleActions } from 'redux-actions';
import session, {
  socketStatus as statuses,
  connect,
  pubWatchdog,
  pubData,
  auth,
  readChannels,
  disconnect,
} from '../services/tiip-session';
import sensors from '../services/sensors';
import conf from '../../client.config';

const defaultState = fromJS({
  username: '',
  serverIp: '',
  socketStatus: statuses.notConnected,
  thingRid: '',
  channels: Map(),
  sensors: sensors.map(() => fromJS({
    active: false,
    last: {},
  })),
});

// ================================================================================================
//  UTILS

// ================================================================================================
//  ACTIONS TO REDUCE ON

export const setUsername = createAction('app/USERNAME');
export const setServerIp = createAction('app/SERVERIP');
const setThingRid = createAction('app/THINGRID');
const setChannels = createAction('app/CHANNELS');
const setSocketStatus = createAction('app/SOCKETSTATUS');
const activateSensor = createAction('app/ACTIVATE');
const setDataPoint = createAction('app/SETDATAPOINT');
export const setSensorExtra = createAction('app/SETSENSOREXTRA');
// export const setSensorExtra = (sensor, key, data) => (dispatch) => {

// ================================================================================================
//  COMPLEX ACTIONS

const handleError = err => (dispatch) => {
  session.logout();
  dispatch(setSocketStatus(statuses.unableToConnect));
  throw err;
};

export const appConnect = (serverIp, username, secure) => async (dispatch, getState) => {
  if (getState().app.get('socketStatus') === 1) {
    const result = await disconnect();
    return result;
  }
  const c = conf.backend;
  let thingRid;
  let channels;
  try {
    connect(serverIp,
      () => dispatch(setSocketStatus(statuses.connected)), // onRelogin
      () => dispatch(setSocketStatus(statuses.closed)), // onClose
      secure,
    );
    thingRid = await auth(c.userId, c.password, c.tenant);
    // console.log('thingRid:', thingRid);
    dispatch(setThingRid(thingRid));
  } catch (err) {
    handleError(err);
  }
  try {
    channels = await readChannels(thingRid);
    dispatch(setChannels(channels));
  } catch (err) {
    handleError(err);
  }
  try {
    await pubWatchdog(username, channels.getIn(['watchdog', 'rid']), true);
    dispatch(setSocketStatus(statuses.connected));
  } catch (err) {
    handleError(err);
  }
  return true;
};

export const receiveDataPoint = (point, sensor) => (dispatch, getState) => {
  let sensorId = sensor;
  if (sensor === 'camera') sensorId = 'video';
  const channel = getState().app.getIn(['channels', sensorId, 'rid']);
  pubData(
    point,
    channel,
    getState().app.get('username'),
  );
  dispatch(setDataPoint({ sensor, point }));
};

export const toggleSensor = (sensor, active) => (dispatch) => {
  dispatch(activateSensor({ sensor, active }));
  // console.log('Toggle:', sensors.getIn([sensor, 'sensor', 'init']));
  if (active) {
    return sensors.getIn([sensor, 'sensor', 'init'])(
      (point, sensorId) => {
        // console.log('GOT DATA:', sensorId, point);
        dispatch(receiveDataPoint(point, sensorId));
      },
    );
  }
  dispatch(setDataPoint({ sensor, point: Map() }));
  return sensors.getIn([sensor, 'sensor', 'stop'])();
};

// ================================================================================================
//  REDUCER

export default handleActions({
  [setUsername]: (state, action) =>
    state.set('username', action.payload),
  [setServerIp]: (state, action) =>
    state.set('serverIp', action.payload),
  [setThingRid]: (state, action) =>
    state.set('thingRid', action.payload),
  [setChannels]: (state, action) =>
    state.set('channels', action.payload),
  [setSocketStatus]: (state, action) => {
    // console.log('Connection Status:', action.payload);
    return state.set('socketStatus', action.payload);
  },
  [activateSensor]: (state, action) =>
    state.setIn(
      ['sensors', action.payload.sensor, 'active'],
      action.payload.active,
    ),
  [setDataPoint]: (state, action) => {
    // console.log('setDataPoint', action);
    return state.setIn(
      ['sensors', action.payload.sensor, 'last'],
      action.payload.point,
    );
  },
  [setSensorExtra]: (state, action) => {
    // console.log('setDataPoint', action);
    return state.setIn(
      ['sensors', action.payload.sensor, action.payload.key],
      action.payload.data,
    );
  },
}, defaultState);
