import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SensorList from '../components/SensorList';
import { toggleSensor, setSensorExtra } from '../ducks/app';

export default connect(
  state => ({
    sensors: state.app.get('sensors'),
  }),
  dispatch => bindActionCreators({
    toggleSensor,
    setSensorExtra,
  }, dispatch),
)(SensorList);
