import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ConnectionPanel from '../components/ConnectionPanel';
import { appConnect, setUsername, setServerIp } from '../ducks/app';

export default connect(
  state => ({
    username: state.app.get('username'),
    serverIp: state.app.get('serverIp'),
    socketStatus: state.app.get('socketStatus'),
  }),
  dispatch => bindActionCreators({
    appConnect,
    setUsername,
    setServerIp,
  }, dispatch),
)(ConnectionPanel);
