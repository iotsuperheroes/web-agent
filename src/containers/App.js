import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import App from '../components/App';

export default connect(
  state => ({
    username: state.app.get('username'),
    serverIp: state.app.get('serverIp'),
  }),
  dispatch => bindActionCreators({
  }, dispatch),
)(App);
