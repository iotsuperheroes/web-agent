// babel-polyfill must always be first
import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider as ReduxProvider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise';
import { responsiveStateReducer } from 'redux-responsive';
import injectTapEventPlugin from 'react-tap-event-plugin';
import runtime from 'serviceworker-webpack-plugin/lib/runtime';
// import history from './custom-history';
import * as ducks from './ducks';
import App from './containers/App';

// Material UI needs this polyfill
injectTapEventPlugin();

// ========================================================
//  REDUX

const enhancers = [];
if (window.devToolsExtension) { // eslint-disable-line
  enhancers.push(window.devToolsExtension()); // eslint-disable-line
}
const createStoreWithMiddleware = compose(
  applyMiddleware(thunk),
  applyMiddleware(promiseMiddleware),
  ...enhancers,
)(createStore);
const reducer = combineReducers({
  ...ducks,
  browser: responsiveStateReducer,
});
const store = createStoreWithMiddleware(
  reducer,
);

// ========================================================
//  SERVICE WORKER

// if ('serviceWorker' in navigator) {
//   window.addEventListener('load', () => {
//     navigator.serviceWorker.register('sw.js');
//   });
// }

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    const registration = runtime.register();
    if (!registration) navigator.serviceWorker.register('sw.js');
  });
}

// ========================================================
//  DOM RENDER

console.log('client.jsx render...');
ReactDOM.render(
  <ReduxProvider store={store}>
    <App />
  </ReduxProvider>,
  document.getElementById('app'), // eslint-disable-line
);
