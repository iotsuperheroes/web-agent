import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';
import autoprefixer from 'autoprefixer';
import precss from 'precss';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import ServiceWorkerWebpackPlugin from 'serviceworker-webpack-plugin';
import clientConf from './client.config';

let TARGET = 'development';
if (process.env.NODE_ENV) TARGET = process.env.NODE_ENV.trim();

const PATHS = {
  src: path.resolve('./src'),
  build: path.resolve('./dist'),
  modules: path.resolve('./node_modules'),
  assets: 'assets',
  // assets: (TARGET === 'production' ? '/assets' : 'assets'),
};

const common = {
  context: __dirname,
  resolve: {
    modulesDirectories: [PATHS.modules],
    root: [PATHS.src],
    extensions: ['', '.js', '.jsx'],
  },
  resolveLoader: {
    root: PATHS.modules,
  },
  postcss() {
    return [autoprefixer, precss];
  },
};

const commonLoaders = [
  {
    test: /\.css$/,
    loader: 'style!css?modules',
    include: /flexboxgrid/, // react-flexbox-grid needs to load flexboxgrid with 'modules'
  },
  {
    test: /\.json$/,
    loader: 'json-loader',
  },
  {
    test: /\.jsx?$/,
    loader: 'babel-loader',
    exclude: [PATHS.modules],
  },
  {
    test: /\.(svg)(\?v=[0-9]\.[0-9]\.[0-9])$/,
    loaders: [
      `file?hash=sha512&digest=hex&name=${PATHS.assets}/images/[hash:base58:8].[ext]`,
      'img?minimize&optimizationLevel=5&progressive=true',
    ],
    include: [PATHS.src, PATHS.modules],
  },
  {
    test: /\.(jpe?g|png|gif|svg)$/i,
    loaders: [
      `file?hash=sha512&digest=hex&name=${PATHS.assets}/images/[hash:base58:8].[ext]`,
      'img?minimize&optimizationLevel=5&progressive=true',
    ],
    include: [PATHS.src],
  },
  {
    test: /\.(woff|woff2|eot|ttf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    loader: `url-loader?limit=10000&mimetype=application/font-woff&name=${PATHS.assets}/fonts/
      [name].[ext]`,
    include: [PATHS.src, PATHS.modules],
  },
];

const cssLoaders = [
  'css-loader?modules&importLoaders=2&localIdentName=[name]__[local]___[hash:base64:5]',
  'postcss-loader',
];

const plugins = [
  new webpack.optimize.OccurenceOrderPlugin(),
  new HtmlWebpackPlugin({
    template: './src/static/index.html',
    title: clientConf.title,
    favicon: './src/static/favicon.ico',
  }),
  new CopyWebpackPlugin([
    { from: 'manifest.json', to: 'manifest.json' },
  ]),
  new ServiceWorkerWebpackPlugin({
    entry: path.join(__dirname, './src/sw.js'),
  }),
];

const targets = {
  test: {
    ...common,
    devtool: 'inline-source-map',
  },

  development: {
    ...common,
    devtool: 'cheap-module-source-map',
    entry: [
      'webpack-hot-middleware/client',
      './src/client.jsx',
    ],
    output: {
      path: path.join(__dirname, 'dist'),
      publicPath: '/',
      filename: 'client.[hash].js',
    },
    module: {
      loaders: commonLoaders.concat([
        {
          test: /\.p?css$/,
          include: [PATHS.src],
          loaders: ['style-loader'].concat(cssLoaders),
        },
        {
          test: /\.css$/,
          include: [PATHS.modules],
          exclude: /flexboxgrid/,
          loader: 'style-loader!css-loader',
        },
      ]),
      noParse: ['ws'],
    },
    externals: ['ws'],
    node: {
      fs: 'empty',
    },
    plugins: plugins.concat([
      new webpack.HotModuleReplacementPlugin(),
    ]),
  },

  production: {
    ...common,
    devtool: 'source-map',
    entry: {
      client: './src/client.jsx',
    },
    output: {
      path: path.join(__dirname, 'dist'),
      publicPath: clientConf.basename,
      filename: '[name].js',
    },
    module: {
      loaders: commonLoaders.concat([
        {
          test: /\.p?css$/,
          include: [PATHS.src],
          loader: ExtractTextPlugin.extract('style-loader', cssLoaders),
        },
        {
          test: /\.css$/,
          include: [PATHS.modules],
          exclude: /flexboxgrid/,
          loader: ExtractTextPlugin.extract('style-loader', 'css-loader'),
        },
      ]),
    },
    plugins: plugins.concat([
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': '"production"',
      }),
      new ExtractTextPlugin('styles.css', { allChunks: true }),
      new webpack.optimize.UglifyJsPlugin({
        mangle: false,
        compress: {
          dead_code: true,  // discard unreachable code
          unsafe: false, // some unsafe optimizations
          unused: false, // drop unused variables/functions
          hoist_vars: false, // hoist variable declarations
          side_effects: false, // drop side-effect-free statements
          global_defs: {},
        },
      }),
      new webpack.NoErrorsPlugin(),
    ]),
  },
};

export default targets[TARGET] || targets.development;
